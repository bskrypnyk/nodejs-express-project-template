var path = require( 'path' );

module.exports = {
	"db" : {
		"hostname" : "TODO: hostname",
		"database" : "TODO: database"
	}
	,
	"smtp" : {
		"host" : "localhost",
		"port" : 25
	}
	,
	"server" : {
		"port" : 3000,
		"ssl_port" : 3443,
		"ssl"  : false,
		"ssl_pem" : "ssl" + path.sep + "localhost.pem",
		"ssl_key" : "ssl" + path.sep + "localhost.key",
		"ssl_crt" : "ssl" + path.sep + "localhost.crt",
	}
	,
	"app" : {
		"static_resource_max_age" : 1000 * 60 * 60 * 12,
		"upload_dir" : path.sep + "tmp" + path.sep + "web-upload",
		"upload_max_size_mb" : 50,
		"support_email" : "TODO: support@mycompany.com",
	}
};