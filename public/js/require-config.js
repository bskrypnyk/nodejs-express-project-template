/**
	bill: note that all paths are relative, it's easier that way, we 
	can always control "relative to what" via the baseUrl attribute
	this makes it easier when using optimizer as the paths are always going to be relative to the baseUrl
	which can be different in optimizer's app.build.js (as in the case of a top-level "public" directory
	that hosts all the js resources)
*/
require.config( {
	
	baseUrl 	: "/",
	paths 		: {
		// note: no .js extensions here
		"text"              : 	"js/_requirejs/text",
		"tmpl"              :   "js/_common/tmpl-require-plugin",
		"i18n"              :   "js/_requirejs/i18n",
		"underscore"        :	"js/_external/underscore-min",
		"jquery"            :	"js/_external/jquery-1.7.1.min",
		"jquery_form"       : 	"js/_external/jquery.form",
		"jquery_transit"    :	"js/_external/jquery.transit.min",
		"jquery_pulse"      : 	"js/_external/jquery.pulse.min",
		"jquery_cookies"    : 	"js/_external/jquery.cookies.2.2.0.min",
		"json2"             :	"js/_external/json2",
		"bootstrap"         :	"bootstrap/js/bootstrap.min",
		"backbone"          :	"js/_external/backbone",
		"locale"            :	"js/_common/locale",
		"common"            :	"js/_common/common",
		"swig"              :	"js/_external/swig",
		"fileupload-custom" :	"js/_common/fileupload-custom"
	}
	,
	shim 		: {
		"jquery_form"       : 	[ "jquery" ], 
		"jquery_transit"    : 	[ "jquery" ],
		"jquery_pulse"      : 	[ "jquery" ],
		"jquery_cookies"    : 	[ "jquery" ],
		"bootstrap"         :	[ "jquery" ],
		"tmpl"              :   [ "module", "jquery", "swig" ],
		"swig"              :	[ "underscore" ],
		"backbone"          : 	[ "underscore", "jquery", "json2" ],
		"common"            : 	[ "jquery_cookies", "jquery_pulse", "jquery_transit", "jquery_form", "bootstrap", "backbone" ]
	}
});