define( [ 
	"text", 
	"underscore",
	"jquery",
	"json2", 
	"jquery_cookies",
	"jquery_pulse",
	"jquery_transit",
	"jquery_form",
	"bootstrap",
	"backbone",
	"locale",
	"tmpl", // swig only
	"common" ], 
	
	function( 
		text, 
		_underscore, 
		_jQuery, 
		_json2, _jquery_cookies, _jquery_pulse, _jquery_transit, _jquery_form, _bootstrap, /* all placeholders, will be undefined */
		_backbone, /* just a placeholder, notice, lowercase 'b', Backbone (capitabl 'B') is a global variable; not using AMD-wrapped backbone here */
		_locale, /* just a placeholder, will be undefined; injects self into global window namespace */
		_tmpl,
		common
		) {
				
				
	var $ = jQuery;
					
	if( typeof window.console == 'undefined' ) {
		window.console = {
			log : function( msg ) {}
		}
	}
	
	window.common = common;
			
		
	// initialize the app
	
	var BaseModel = Backbone.Model.extend( {
		
		// custom base model for the application
		
		hasLocalProgress : false		
	});
	
	var BaseCollection = Backbone.Collection.extend( {
		
		// custom base collection for the application
		
		hasLocalProgress : false
	});
	
	var BaseView = Backbone.View.extend( {
	
		progressDelayMillis : 500,
		showReloadOnError : true,
			
		title : function() {
			return null;
		},
		
		loadingMessage : function() {
			return "Loading...";	
		},
		
		loadingRequest : 0,
		
		_beforeConfigure : function() {},
		_afterConfigure : function() {},
		_afterSetElement : function() {},
		
		parseJSON : function( data ) {
		
			if( !data ) {
				return {};
			}
			
			try {
				return JSON.parse( data );
			}
			catch( e ) {
				console.log( e );
				return {};
			}
		},
		
		fetch : function( model ) {
		
			var m = model || this.model || this.collection;
			
			if( m && m.isNew && m.isNew() ) {
				return;	
			};
			
			var _this = this;
			
			_this.showLoadProgress();
			
			m.fetch( {
				error : function( model, response ) {
				
					_this.loadingRequest = null;
					
					var errorMessage = "Sorry, an unexpected server error has occurred. Please try later.";
					
					if( response.responseText ) {
						try {
							var data = _this.parseJSON( response.responseText );
							if( data && data.errorMessage ) {
								errorMessage = data.errorMessage;
							}
						}
						catch( e ) {
						}
					}
					
					_this.showLoadError( errorMessage );
		
				},
				success : function( model, response ) {
					_this.loadingRequest = null;
					_this.$el.find( ".load-progress" ).remove();
					_this.$el.css( "opacity", 0 );
					_this.render();
					_this.$el.transition( { opacity : 1 } );
				}
			});
			
		},
		
		showLoadProgress : function() {
		
			var _this = this;
			
			var loadingRequest = ( new Date().getTime() ) + "-" + Math.random();
			
			_this.loadingRequest = loadingRequest;
			
			setTimeout( function() {
			
				if( _this.loadingRequest != loadingRequest ) { // things changed, bail
					return;
				}	
			
				var progress = $( "<div class='view-load-progress'><div class='load-progress progress progress-striped active'>" +
					"<div class='bar progress-info' style='width: 100%;'></div></div><div class='view-load-message'></div></div>" );
					
				progress.find( ".view-load-message" ).text( _this.loadingMessage() || "Loading..." );
				_this.$el.empty().append( progress );
				
			}, this.progressDelayMillis );
		
			
		},
		
		
		showLoadError : function( errorMessage ) {
				
			var errorBlock = $( "<div></div>" );
			errorBlock.addClass( "view-error" );
			
			var alert = $( "<div></div>" );
			alert.addClass( "alert alert-error" );
			
			/*
var dismiss = $( "<button type='button'>&times;</button>" );
			dismiss.addClass( "close" );
			dismiss.attr( "data-dismiss", "alert" );
			alert.append( dismiss );
*/
			
			var msg = $( "<span></span>" );
			msg.addClass( "load-error" );
			if( errorMessage ) {
				msg.html( errorMessage );
			}
			else {
				msg.text( "Sorry, an unexpected error occurred while loading... Please try again later" );
			}
			
			alert.append( msg );
			
			errorBlock.append( alert );
						
			if( this.showReloadOnError ) {
				var reload = $( "  <a href='javascript:;' class='reload'>Try again</a>" );
				reload.on( "click", function() {
					window.location.reload();
				});
				msg.append( reload );
			}
			
			
			this.$el.empty().append( errorBlock );
		}
	});
	
	BaseView.prototype._super_configure = BaseView.prototype._configure;
	
	BaseView.prototype._configure = function( options ) {

		var _this = this;
		
		_this._beforeConfigure( options );	
		_this._super_configure( options );
		_this._afterConfigure( options );
		
		// here we wrap around the render method, since
		// we are doing this after instantiation, we have access to the
		// sub-class's render() method
		// here we can trigger custom events, etc
		var _render = this.render;	
		var _setElement = this.setElement;	
		
		_this.setElement = function() {
			_setElement.apply( this, arguments );
			_this.trigger( "element-set", this );
			_this._afterSetElement.apply( this, arguments );
		};
		
		_this.render = function() {
			_render.apply( this, arguments );
			_this.trigger( "rendered", this );
		};
		
		_this.trigger( "configured", this );
	};
		
	var BaseFormView = BaseView.extend( {
	
		progressIndicatorDelay : 500,
		_wasNew : false,
	
		events : {
			"keydown input"  : "onEnterKey" 
		},
		
		getFieldsToSubmit : function() {}, // must override for submit, return { field: val }
		beforeSubmit : function() {}, // to be overriden
		submitError  : function( model, xhr ) {}, // to be overriden
		submitSuccess : function( model, data ) {}, // to be overriden
		
		onEnterKey : function( e ) {
		
			if( e && e.keyCode == 13 ) {
				e.stopPropagation();
				e.preventDefault();
				this.$el.find( ".submit" ).eq(0).trigger( "click" );
				return false;
			}
		},
		
		prepareFormForSubmit : function() {
			var _this = this;
			this.$el.find( ".progress-indicator" ).attr( "data-delayed", "true" );
			
			setTimeout( function() {
				if( _this.$el.find( ".progress-indicator" ).attr( "data-delayed" ) === "true" ) {
					_this.$el.find( ".progress-indicator" ).removeClass( "invisible" );
				}
			}, this.progressIndicatorDelay);
			
			this.$el.find( ".alert" ).html( "&nbsp;" ).addClass( "invisible" );
			common.clearErrorsFromFormControls( this.$el );
			this.$el.find( ".submit" ).attr( "disabled", "disabled" );
		},
		
		restoreFormAfterSubmit : function() {
			this.$el.find( ".progress-indicator" ).removeAttr( "data-delayed" )
			this.$el.find( ".progress-indicator" ).addClass( "invisible" );
			this.$el.find( ".submit" ).removeAttr( "disabled" );
		},
		
		_onError : function( model, xhr ) {
						
			this.restoreFormAfterSubmit();
			
			var data = {};
			if( !xhr.responseText ) {
				data = {
					errorMessage : "Sorry, an unexpected server error has occurred. Please try later."
				}
			}
			else {
				data = this.parseJSON( xhr.responseText );
			}
			 
			if( data.errors ) {
				common.addErrorsToFormControls( this.$el, data.errors, {
					placement : this.options.fieldErrorPlacement ? this.options.fieldErrorPlacement : "right"
				});
			}
			if( data.errorMessage ) {
				this.$el.find( ".alert" ).html( data.errorMessage ).
					removeClass( "invisible" );
			}
			this.submitError( model, xhr );
		},
		
		_onSuccess : function( model, data ) {
			this.restoreFormAfterSubmit();
			this.submitSuccess( model, data );
		},
		
		wasNew : function() {
			return this._wasNew;	
		},
	
		submit : function() {
	
			var _this = this;
			
			if( !this.model ) {
				console.log( "cannot submit: no model" );
				return;
			}
		
			_this.prepareFormForSubmit();
			
			if( _this.model ) {
			
				_this._wasNew = _this.model.isNew();
			
				_this.model.save( this.getFieldsToSubmit(), {
					
					wait : true,
					error : function( model, xhr ) {
						_this._onError( model, xhr );
					}
					,
					success : function( model, xhr ) {
						_this._onSuccess( model, xhr );
						model.trigger('sync', model, xhr);
					}
				});
			}
			
		}
	});
	
	var BaseCollectionView = BaseView.extend( {
	
		
	});
	
	var syncSuper = Backbone.sync;
	
	Backbone.sync = function( method, model, options ) {
		
		options = options || {};
		
		var callerError = options.error;
		var callerSuccess = options.success;
				
		options.error = function() {
		
			if( !model.hasLocalProgress ) {
				App.hideProgress( model );
			}
			
			if( callerError ) {
				callerError.apply( null, arguments );
			}
		};
		
		options.success = function() {
			if( !model.hasLocalProgress ) {
				App.hideProgress( model );
			}
			
			if( callerSuccess ) {
				callerSuccess.apply( null, arguments );
			}	
		};
		
		if( !model.hasLocalProgress ) {
			App.showProgress( model );
		}
				
		syncSuper( method, model, options );	
	};
	
	( function() {
				
		Backbone.History.prototype._loadUrl = Backbone.History.prototype.loadUrl;
		
		Backbone.History.prototype.routeInterceptors = [];
		
		Backbone.History.prototype.pushRouteInterceptor = function( interceptor ) {
			this.routeInterceptors.push( interceptor );
		};
				
		Backbone.History.prototype.loadUrl = function(fragmentOverride) {
			
			var fragment = this.getFragment(fragmentOverride);
			
			for( var i=0; i<this.routeInterceptors.length; ++i ) {
				if( this.routeInterceptors[i]( fragment ) ) {
					return true;
				}
			}
						
			return this._loadUrl( fragmentOverride );
		};

		
		Backbone.History.prototype.withRoot = function( url ) {
			
			url = url || "";
			
			if( this.options.root ) {
				if( url.indexOf( this.options.root) == 0 ) {
					return url;
				}
				if( this.options.root.match( /.+\/$/ ) ) {
					if( url.indexOf( '/' ) == 0 ) {
						return this.options.root + url.substring(1);
					}
					else {
						return this.options.root + url;
					}
				}
				else {
					this.options.root + '/' + url;
				}
			}
			return url;
		};
		
		Backbone.History.prototype.withoutRoot = function( url ) {
		
			url = url || "";
			
			if( this.options.root ) {
				if( url.indexOf( this.options.root) == 0 ) {
					if( this.options.root.match(  /.+\/$/ ) ) {
						return url.replace( this.options.root, "/" );
					}
					else {
						return url.replace( this.options.root, "" );
					}
				}
			}
			return url;
		};
		
				
		Backbone.History.prototype.navigateURL = function( url, silent ) {
		
			var prev = window.location.pathname + ( window.location.search || "" );
		
			url = this.withoutRoot( url );
			
			this.navigate( url, { trigger : !silent } );

			this.trigger( "navigateURL", {
				before : prev,
				after: this.withRoot( url )
			});
		};
		
		Backbone.History.prototype.replaceURL = function( url, silent ) {
		
			var prev = window.location.pathname + ( window.location.search || "" );
		
			var url = this.withoutRoot( url );
			
			this.navigate( url, { replace: true, trigger : !silent } );
			
			this.trigger( "replaceURL", {
				before : prev,
				after: this.withRoot( url )
			});
		};
		
	})();
	
	
		
	var HistoryLayout = BaseView.extend( {
			
		currentIndex : -1,
		views : [],
		history: [],
		visibleBreadcrumbs : 5,
		
		createStack : function() {
			var out = document.createElement( "div" );
			out.className = "stack-item";
			this.$container.append( out );
			return out;	
		},
		
		initialize : function( options ) {	
		},
		
		addView : function( view ) {
		
			var _this = this;
				
			if( this.currentIndex < this.views.length - 1 ) {
				this._trimStack( this.currentIndex );
			}
			
			var m = view.model || view.collection;
			
			if( m ) {
				m.on( "change", function() {
					_this._navigated();
				});
			}
								
			var current = _.last( this.views );
			
			_this.views.push( {
			
				url : window.location.pathname + ( window.location.search ? window.location.search : "" ),
				title : view.title(),
				view: view
			} );
			
			this.history.push( {
				url : window.location.pathname + ( window.location.search ? window.location.search : "" ),
				title: view.title(),
				view : view
			});
			
			if( current && current.view ) {
				this._hideView( current.view );
			}
					
			var stack = this.createStack();
			
			$(stack).empty().append( view.$el );
			
			this._revealView( view );
						
			this.currentIndex++;
			
			this._navigated();
			
			this._prune();
		},
		
		back : function() {
			this.gotoIndex( this.currentIndex - 1 );		
		},
		
		forward : function() {
			this.gotoIndex( this.currentIndex + 1 );
		},
		
		checkRouteInHistory : function( fragment ) {
								
			var url = Backbone.history.options.root + fragment;
			
			for( var i=this.views.length-1; i>=0; --i ) {
				if( this.views[i] && this.views[i].url === url ) {
					this.gotoIndex( i );
					return true;
				}
			}	
			return false;
		},
		
		gotoIndex : function( index ) {
		
			if( index < 0 || index > this.views.length - 1 ) {
				return;
			}
							
			var previous = this.currentIndex;
			this.currentIndex = index;
			
			var entry = this.views[previous];
			if( entry && entry.view ) {
				this._hideView( entry.view );
			}
			
			var currentEntry = this.views[this.currentIndex];
			if( currentEntry && currentEntry.view ) {
				this._revealView( currentEntry.view );
			}
			
			if( Backbone.history ) {
				Backbone.history.navigate( Backbone.history.withoutRoot( currentEntry.url ), { trigger: false, replace: true });
			}
			
			this._navigated();
		},
		
		updateBreadcrumbs : function() {
					
			var _this = this;
			
			var breadcrumbsStartIndex = -1;
			
			// get current crumb
			if( this.views.length > 0 ) {

				var crumbVisible = false;
				
				var currentView = this.views[this.currentIndex];
				if( currentView ) {
				
					var saved = _this.$breadcrumbs.data( "breadcrumbsStartIndex" );
					if( isNaN(saved) ) {
						saved = -1;
					}
					breadcrumbsStartIndex = saved;
					
					this.$breadcrumbs.find( "a.visible-crumb" ).each( function(idx) {
						if( $(this).attr( "href" ) === currentView.url ) {
							
							crumbVisible = true;
														
							if( idx == 0 && breadcrumbsStartIndex >= 0 && breadcrumbsStartIndex > 0  ) {
								breadcrumbsStartIndex--;
							}
							else if( idx >= _this.visibleBreadcrumbs -1 && breadcrumbsStartIndex >= 0 && 
								breadcrumbsStartIndex + _this.visibleBreadcrumbs < _this.views.length) {
								breadcrumbsStartIndex++;
							}
							
							return false;
						}
					});
				}
				
				if( !crumbVisible ) {
					breadcrumbsStartIndex = -1;
				}
			}
			
					
			this.$breadcrumbs.empty();
			
			if( this.views.length == 0 ) {
				return;
			}
			
			
			if( breadcrumbsStartIndex == -1 ) {
				if( this.views.length - this.currentIndex >= this.visibleBreadcrumbs ) {
					breadcrumbsStartIndex = this.currentIndex - 1;
				}
				else {
					breadcrumbsStartIndex = this.views.length - this.visibleBreadcrumbs;
				}
				
				if( breadcrumbsStartIndex < 0 ) {
					breadcrumbsStartIndex = 0;
				}
			}	
			
			this.$breadcrumbs.data( "breadcrumbsStartIndex", breadcrumbsStartIndex );		
			
			if( breadcrumbsStartIndex > 0 ) {
				var li = $( "<li></li>" );
				li.addClass( "dropdown left" );
				
				var a = $( "<a></a>" );
				a.addClass( "dropdown-toggle" );
				a.attr( "role", "button" );
				a.attr( "data-toggle", "dropdown" );
				a.attr( "data-target", "#" );
				a.attr( "href", "javascript:;" );
				a.text( "..." );
				li.append( a );
				
				var dropdown = $( "<ul></ul>" );
				dropdown.addClass( "dropdown-menu" );
				dropdown.attr( "role", "menu" );
				li.append( dropdown );
				
				for( var i=breadcrumbsStartIndex-1; i>=0; --i ) {
					var item = $( "<li></li>" );
					var a = $( "<a></a>" );
					a.attr( "tabindex", "-1" );
					a.attr( "href", this.views[i].url );
					a.text( this.views[i].view.title() );
					a.on( "click", function(e) {
						e.preventDefault();
						e.stopPropagation();
						Backbone.history.navigateURL( $(this).attr("href") );
						return false;
					});

					item.append( a );
					dropdown.append( item );
				}
				
				this.$breadcrumbs.append( li );
			}
			
			for( var i=breadcrumbsStartIndex; i<this.views.length && i < (breadcrumbsStartIndex + this.visibleBreadcrumbs); ++i ) {
			
				var li = $( "<li></li>" );
				
				if( i > 0 ) {
					li.append( $( "<span class='divider'>/</span>" ) );
				}
				
				
				var a = $( "<a></a>" );
				a.attr( "href", this.views[i].url );
				a.addClass( "visible-crumb" );
				a.text( this.views[i].view.title() );
				a.on( "click", function(e) {
					e.preventDefault();
					e.stopPropagation();
					Backbone.history.navigateURL( $(this).attr("href") );
					return false;
				});
				li.append( a );
				
				if( this.views[i].url == this.views[this.currentIndex].url ) {
					li.addClass( "active" );
					a.addClass( "active" );
				}
				
				this.$breadcrumbs.append( li );
			}
			
			if( breadcrumbsStartIndex + this.visibleBreadcrumbs < this.views.length ) {
			
				var li = $( "<li></li>" );
				li.addClass( "dropdown right" );
				
				li.append( $( "<span class='divider'>/</span>" ) );
				
				var a = $( "<a></a>" );
				a.addClass( "dropdown-toggle" );
				a.attr( "role", "button" );
				a.attr( "data-toggle", "dropdown" );
				a.attr( "data-target", "#" );
				a.attr( "href", "javascript:;" );
				a.text( "..." );
				li.append( a );
				
				var dropdown = $( "<ul></ul>" );
				dropdown.addClass( "dropdown-menu" );
				dropdown.attr( "role", "menu" );
				li.append( dropdown );
				
				for( var i=breadcrumbsStartIndex + this.visibleBreadcrumbs; i<this.views.length; ++i ) {
					var item = $( "<li></li>" );
					var a = $( "<a></a>" );
					a.attr( "tabindex", "-1" );
					a.attr( "href", this.views[i].url );
					a.text( this.views[i].view.title() );
					a.on( "click", function(e) {
						e.preventDefault();
						e.stopPropagation();
						Backbone.history.navigateURL( $(this).attr("href") );
						return false;
					});

					item.append( a );
					dropdown.append( item );
				}


				this.$breadcrumbs.append( li );
			}
		},
		
		updateNavigation : function() {
		
			if( this.views.length == 0 ) {
				this.$buttons.find( "a" ).attr( "disabled" );
				return;
			}
			
			this.$buttons.find( "a" ).removeAttr( "disabled" );
		
			if( this.currentIndex == this.views.length - 1 ) {
				this.$buttons.find( ".next" ).attr( "disabled", "disabled" );
			}
			else {
				this.$buttons.find( ".next" ).removeAttr( "disabled" );
			}
			
			if( this.currentIndex == 0 ) {
				this.$buttons.find( ".prev" ).attr( "disabled", "disabled" );
			}
			else {
				this.$buttons.find( ".prev" ).removeAttr( "disabled" );
			}
			
			var v = this.views[this.currentIndex];
			if( v && v.view && v.view.model && v.view.model.isNew() ) {
				this.$buttons.find( ".reload" ).attr( "disabled", "disabled" );
			}
			else {
				this.$buttons.find( ".reload" ).removeAttr( "disabled" );
			}
		},
		
		updateControls : function() {
			this.updateBreadcrumbs();
			this.updateNavigation();	
		},
		
		_navigated : function() {
			this._dumpViews();
			this.updateControls();
		},
		
		_dumpViews : function() {
			console.log( "++++++++++++++++++++++++++++" );
			for( var i=0; i<this.views.length; ++i ) {
				console.log( this.views[i].url );
			}
			console.log( "++++++++++++++++++++++++++++" );
		},
		
		_prune : function() {
			
		},
		
		showHistory : function() {
			
		},
		
		_trimStack : function( fromIndex ) {
			for( var i=fromIndex+1; i<this.views.length; ++i ) {
				this._destroyView( i );
			}
			this.views = this.views.slice( 0, fromIndex+1 );
		},
		
		_destroyView : function( index ) {
			var v = this.views[index];
			if( v && v.view) {
				try {
					v.view.$el.parent().remove();
					v.view.remove();
					this.views[index] = null;
				}
				catch( e ) {
				}
			}	
		},
		
		_hideView : function( view ) {
			if( !view ) {
				return;
			}
			view.$el.parent().hide();
		},
		
		_revealView : function( view ) {
		
			view.$el.parent().show();
		},
		
		_removeURL : function( url ) {
		
			var found = -1;
			for( var i=0; i<this.views.length; ++i ) {
				if( this.views[i].url === url ) {
					found = i;
					this.views[i] = null;
				}
			}
			
			if( found < 0 ) {
				return;
			}
			
			if( found < this.currentIndex ) {
				this.currentIndex--;
			}
			
			this.views = _.compact( this.views );
			
			this._navigated();
		},
		
		_afterSetElement : function() {
			
			var _this = this;
					
			if( !Backbone.history ) {
				return;
			}

			Backbone.history.on( "replaceURL", function( e ) {
				_this._removeURL( e.before );
			});	
		},
		
				
		render : function() {	
		
			var _this = this;						
						
			this.$toolbar = $( "<div></div>" );
			this.$toolbar.addClass( "stack-toolbar" );
			
			this.$buttons = $( "<div></div>" );
			this.$buttons.addClass( "btn-toolbar" );			
			
			var $reload = $( "<a class='btn btn-small reload' href='javascript:;'><i class='icon-repeat'></i></a>" );
			$reload.on( "click", function( e ) {
				e.preventDefault();
				
				if( _this.currentIndex >= 0 ) {
				
					var entry = _this.views[_this.currentIndex];
					if( entry && entry.view && entry.view.fetch ) {						
						entry.view.fetch();
						return false;
					}
				}
				
				window.location.reload();
				return false;
			});
			this.$buttons.append( $reload );
						
			var $popout = $( "<a class='btn btn-small new-window' href='javascript:;'><i class='icon-fullscreen'></i></a>" );
			$popout.on( "click", function( e ) {
				e.preventDefault();
				var ww = $(window).width();
				var wh = $(window).height();
				window.common.new_window( window.location.href, ww, wh, Math.floor( Math.random() * 1000 ) + "" );
				return false;
			});
			this.$buttons.append( $popout );
			
			var $nav = $( "<div class='btn-group'></div>" );
			this.$buttons.append( $nav );
			
			var $prev = $( "<a class='btn btn-small prev' href='javascript:;'><i class='icon-chevron-left'></i></a>" );
			$prev.on( "click", function( e ) {
				e.preventDefault();
				_this.back();
				return false;
			});
			$nav.append( $prev );
						
			var $next = $( "<a class='btn btn-small next' href='javascript:;'><i class='icon-chevron-right'></i></a>" );
			$next.on( "click", function( e ) {
				e.preventDefault();
				_this.forward();
				return false;
			});
			$nav.append( $next );
			
			this.$toolbar.append( this.$buttons );
			
			this.$breadcrumbs = $( "<ul></ul>" );
			this.$breadcrumbs.addClass( "breadcrumb" );
			this.$toolbar.append( this.$breadcrumbs );
			
						
			this.$container = $( "<div></div>" );
			this.$container.addClass( "stack" );
			
			this.$el.append( this.$toolbar );
			this.$el.append( this.$container );
			
			if( Backbone.history ) {
				Backbone.history.pushRouteInterceptor( _.bind( this.checkRouteInHistory, this ) );
			}
		}
	});
		
	window.App = _.extend( {
	
		options : {
			globalProgressDefaultMessage    : "Working...",
			globalProgressIndicatorSelector : "#main-progress-indicator",
			globalProgressIndicatorMessageSelector : '',
			globalProgressIndicatorDelay    : 500
		},
		
		views : {},
		models : {},
		
		setProgressContent: function ( el, content ) {
			
			var o = this.options;
			
			if( o.globalProgressIndicatorMessageSelector ) {
				el.find( o.globalProgressIndicatorMessageSelector ).html( content );
			}
			else {
				el.html( content );
			}
		},
		showProgress : function( source, optionalMessage ) {

			var o = this.options;
		
			var p = jQuery( o.globalProgressIndicatorSelector );
			if( p.length == 0 ) {
				return;
			}
			
			if( !source.cid ) {
				source.cid = _.uniqueId('unique');
			}
			
			var queue = p.data( 'progress-queue' );
			if( !queue ) {
				queue = [];
				p.data( 'progress-queue', queue );
			}
			
			var msg = optionalMessage ? optionalMessage : o.globalProgressDefaultMessage;
			
			queue.push( {
				cid : source.cid,
				msg : msg
			});
			
			var _this = this;
			
			function setProgressContent( el, content ) {
				if( o.globalProgressIndicatorMessageSelector ) {
					el.find( o.globalProgressIndicatorMessageSelector ).html( content );
				}
				else {
					el.html( content );
				}
			}
			
			setTimeout( function() {
				var last = _.last( queue );
				if( !last || last.cid !== source.cid ) {
					return;
				}
				if( !p.hasClass( "progress-active" ) ) {
					_this.setProgressContent( p, msg );
					p.addClass( "progress-active" );
					_this.showProgressAnimate( p );
				}
				else {
					_this.setProgressContent( p, msg );
				}
			}, o.globalProgressIndicatorDelay );
		},
		
		hideProgress : function( source ) {
		
			var o = this.options;
		
			var p = jQuery( o.globalProgressIndicatorSelector );
			if( p.length == 0 ) {
				return;
			}
			
			if( !source.cid ) {
				return;
			}
			
			var queue = p.data( 'progress-queue' );
			if( !queue ) {
				queue = [];
				p.data( 'progress-queue', queue );
			}
			
			jQuery.each( queue, function( index, item ) {
				if( !item ) {
					return;
				}
				if( item.cid == source.cid ) {
					queue[index] = null;
					return false;
				}
			});
			
			queue = _.compact( queue );
			p.data( 'progress-queue', queue );
			
			if( queue.length == 0 ) {
				p.removeClass( "progress-active" );
				this.hideProgressAnimate( p );
				return;
			}
			
			var last = _.last( queue );
			this.setProgressContent( p, last.msg );
		},
		
		showProgressAnimate : function( el ) {
			jQuery(el).transition( { top : -2 } );
		},
		
		hideProgressAnimate : function( el ) {
			jQuery(el).transition( { top : -100 } );
		},
	
		BaseModel          : BaseModel,
		BaseCollection     : BaseCollection,
		BaseView           : BaseView,
		BaseFormView       : BaseFormView,
		BaseCollectionView : BaseCollectionView,
		HistoryLayout      : HistoryLayout
		
	}, Backbone.Events );
	
					
	jQuery( function($) {
		
		// DOM ready init
		
		if (!$.support.transition) {
			$.fn.transition = $.fn.animate;
		}
	});	
	
	
	return window.App;
		
});
