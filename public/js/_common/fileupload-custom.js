function enableFileUploadButton( form ) {
	var $ = jQuery;
	$(form).find('.fileinput-button input')
        .prop('disabled', false)
        .parent().removeClass('disabled');	
}

function disableFileUploadButton( form ) {
	var $ = jQuery;
	$(form).find('.fileinput-button input')
        .prop('disabled', true)
        .parent().addClass('disabled');
}


function initSingleFileUploadDialog( dialog, options, successCallback ) {
	
	var $ = jQuery;
	var $dlg = $( dialog ).clone();
	var $form = $dlg.find( "form" );
	
	if( !$.support.xhrFileUpload  ) {
		$form.find( "button.stop" ).addClass( "hide" );
		$form.find( ".progress" ).addClass( "hide" );
	}
	
	var defaults = {
		dropZone: $dlg,
		redirect : window.location.href.replace( /\/[^\/]*$/, "/upload/cors/result.html?%s" ),
		uploadTemplateId : '',
		downloadTemplateId : ''
	};
	$.extend( defaults, options ? options : {} );
	
	$form.fileupload( defaults )
	
	.bind( "fileuploadadded" , function( e, data ) {
		
		$form.find( ".alert" ).addClass("hide");
		
		if( !data.files.valid ) {
			
			$form.find( ".alert p" ).
				html( "Error uploading file <strong>" +
				data.files[0].name +
				"</strong>: "  + locale.fileupload.errors[data.files[0].error] );
				
			$form.find( ".alert" ).removeClass("hide");
			
			return false;
		}
		
		if( !$.support.xhrFileUpload  ) {
			$form.find( "button.stop" ).addClass( "hide" );
			$form.find( ".progress" ).addClass( "hide" );
			$form.find( ".progress-extended" ).css( "text-align", "center").html( "<img src='/img/progress.gif' style='margin-right: 10'> Uploading..." );
			setTimeout( function() {
				data.submit();
			}, 200 );
			return true;
		}
		
		$form.find( ".alert" ).addClass( "hide" );
		
		if( data.files ) {
		
			var jqXHR = $form.fileupload( "send", { files: data.files } );
		
			disableFileUploadButton( $form );
			
			$dlg.
				find( ".stop" ).off( ".fileupload" ).on( "click.fileupload", function(e) {
				if( data.jqXHR ) {
					data.jqXHR.abort();
				}
				if( jqXHR ) {
					jqXHR.abort();
				}
			});	
			
		}
		return true;
	})
	.bind( "fileuploaddone", function( e, data ) {
			
		if( data.result && data.result.length > 0 ) {
			if( successCallback ) {
				successCallback( data.result[0] );
			}
			else {
				successCallback( null );
			}
		}
		else if( successCallback ) {
			successCallback( null );
		}
		setTimeout( function() {
			$dlg.modal( "hide" );
		}, 100 );
	})
	.bind( "fileuploadfail", function( e, data ) {
		if( "abort" != data.errorThrown ) {
			$form.find( ".alert p" ).
				html( "Error uploading file <strong>" +
				data.files[0].name +
				"</strong>: " + data.errorThrown );
			$form.find( ".alert" ).removeClass("hide");
		}		
	})
	.bind( "fileuploadalways", function( e, data ) {		
		$form.find( ".progress-extended" ).html("");
		enableFileUploadButton( $form );
	});
	
	$form.find( ".alert .close" ).click( function(e) {
		e.preventDefault();
		e.stopPropagation();
		$(this).closest( ".alert" ).addClass("hide");
	});
	
	$dlg.on( "hidden", function() {
		$form.fileupload( "destroy" );
		setTimeout( function() {
			$dlg.remove();
		}, 100 );
	});
	
	$dlg.appendTo( $( document.body ) );
	
	return $dlg;
}


