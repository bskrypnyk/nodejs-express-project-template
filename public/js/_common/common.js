define( [ "underscore", "jquery" ], function( _, jQuery ) {
	
	
	function getInternetExplorerVersion() {
	    var rv = -1; // Return value assumes failure.
	    if (navigator.appName == 'Microsoft Internet Explorer') {
	        var ua = navigator.userAgent;
	        var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
	        if (re.exec(ua) != null)
	            rv = parseFloat(RegExp.$1);
	    }
	    return rv;
	}
	
	function getIEVersion() {
		return getInternetExplorerVersion();
	}
	
	function showElement( $elem, animate, callback ) {
		if( animate ) {
			$elem.css( "opacity", 0 ).removeClass( "hide" ).transition( 
				{ opacity: 1 }, 
				200, 
				function() { 
					if( callback ) {
						callback( $elem );
					} 
				}
			);
		}
		else {
			$elem.removeClass( "hide" );
			if( callback ) {
				callback( $elem );
			}
		}
	}
	
	function hideElement( $elem, animate, callback ) {
		if( animate ) {
			$elem.transition( 
				{ opacity: 0 }, 
				200, 
				function() { 
					$elem.addClass( "hide" );
					if( callback ) { 
						callback( $elem ); 
					}
				}
			);
		}
		else {
			$elem.addClass( "hide" );
			if( callback ) {
				callback( $elem );
			}
		}
	}
	
	
	function scrollToTop()
	{
		var $ = jQuery;
		
		var ieVers = getInternetExplorerVersion();
		if( ieVers == -1 ) {
			$( document.body ).animate( {
				scrollTop: 0
			}, 200 );
		}
		else if( ieVers >= 9.0 ) {
			$( "html" ).animate( {
				scrollTop: 0
			}, 200 );
		}
		else {
			$( "html" ).scrollTop(0);
		}
		
	}
	
	function autosize_textarea( textarea ) {
		
		var $ = jQuery;
		
		var $textarea = $(textarea);
		var borders = $textarea.outerHeight(false) - $textarea.innerHeight();
		var padding = $textarea.outerHeight( false ) - $textarea.height();
		$textarea.addClass( "border-box" );
		
		if( textarea ) {
			textarea.style.height = "auto";
			textarea.style.height = ( textarea.scrollHeight  + borders )+ "px";
		}
	}
	
	function new_window( url, width, height, name )
	{
	
		function optimal_height() {
		
			var h = 500;
			if( screen.availHeight > 600 ) {
				h = 700;
			}
			if( screen.availHeight > 800 ) {
				h = 800;
			}
			return h;
		}
	
	
		if( !height ) {
			height = optimal_height();
		}
		
		if( !name ) {
			name = parseInt( Math.random() * 100000 ) + "";
		}
	
	    var left = parseInt((screen.availWidth/2) - (width/2));
	    var top = parseInt((screen.availHeight/2) - (height/2));
		
		if( !name ) {
			name = Math.random();
		}
		var w = window.open( url, name, "width=" + width + ",height=" + height + 
			",scrollbars=1,location=1,left=" + left + ",top=" + top + ",screenX=" + left + ",screenY=" + top );
		
		if( w == null ) {
			alert( "Please enable popups in your browser" );
		}
		else {
			try {
				w.focus();
			}
			catch( e ) {
				
			}
			
		}
		
		return w;
	}
	
	function isEmail(str) {
	        return str.match(/^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/);
	}
	   
	function isURL(str) {
	        return str.match(/^(?:(?:ht|f)tp(?:s?)\:\/\/|~\/|\/)?(?:\w+:\w+@)?(localhost|(?:(?:[-\w\d{1-3}]+\.)+(?:com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|edu|co\.uk|ac\.uk|it|fr|tv|museum|asia|local|travel|[a-z]{2}))|((\b25[0-5]\b|\b[2][0-4][0-9]\b|\b[0-1]?[0-9]?[0-9]\b)(\.(\b25[0-5]\b|\b[2][0-4][0-9]\b|\b[0-1]?[0-9]?[0-9]\b)){3}))(?::[\d]{1,5})?(?:(?:(?:\/(?:[-\w~!$+|.,="'\(\)_\*]|%[a-f\d]{2})+)+|\/)+|\?|#)?(?:(?:\?(?:[-\w~!$+|.,*:]|%[a-f\d{2}])+=?(?:[-\w~!$+|.,*:=]|%[a-f\d]{2})*)(?:&(?:[-\w~!$+|.,*:]|%[a-f\d{2}])+=?(?:[-\w~!$+|.,*:=]|%[a-f\d]{2})*)*)*(?:#(?:[-\w~!$ |\/.,*:;=]|%[a-f\d]{2})*)?$/i) || str.length > 2083;
	}
	
	function _modal_markup()
	{
		var markup = [];
		markup.push( "<div class='modal hide fade'>" );
		markup.push( "<div class='modal-header'><button type=button class='close' data-dismiss='modal'>&times;</button><h3></h3></div>" );
		markup.push( "<div class='modal-body'><p></p></div>" );
		markup.push( "<div class='modal-footer'>" );
		markup.push( "<a href='#' class='btn btn-large modal-cancel' data-dismiss='modal' style='min-width: 100px'>Cancel</a>" );
		markup.push( "<a href='#' class='btn btn-large btn-primary modal-ok'>OK</a>" );
		markup.push( "</div>" ); // .modal-footer
		markup.push( "</div>" ); // .modal .hide
	
		return jQuery( markup.join( "" ) );
	}
	
	function bt_alert ( title, content, okButton, options ) {
		
		var $dlg = _modal_markup();
		
		$dlg.addClass( "modal-alert" );
		
		if( title ) {
			$dlg.find( ".modal-header h3" ).text( title );
		}	
		if( content ) {
			$dlg.find( ".modal-body p" ).html( content );
		}
		
		$dlg.find( "a.modal-ok" ).remove();
		
		$dlg.find( "a.modal-cancel" ).click( function( e ) {
			e.preventDefault();
			if( options && options.okCallback ) {
				options.okCallback( $dlg );
			}
		}).addClass( "btn-primary" ).text( okButton || "OK" );
		
		if( options && options.beforeShowCallback ) {
			$dlg.on( "show", function() {
				options.beforeShowCallback( $dlg );
			});
		}
		
		if( options && options.afterShowCallback ) {
			$dlg.on( "shown", function() {
				options.afterShowCallback( $dlg );
			});
		}
			
		if( options && options.beforeHideCallback ) {
			$dlg.on( "hide", function() {
				options.beforeHideCallback( $dlg );
			});
		}
		
		if( options && options.afterHideCallback ) {
			$dlg.on( "hidden", function() {
				options.afterHideCallback( $dlg );
			});
		}
		
		
		$dlg.on( "hidden", function() {
			setTimeout( function() {
				$dlg.remove();
			}, 100 );
		});
		
		$dlg.appendTo( jQuery( document.body ) );
		
		setTimeout( function() {
			$dlg.modal( "show" );
		}, 100 );
		
		
		$dlg.hide = function() {
			$dlg.modal( "hide" );	
		};
		
		return $dlg;
	}
	
	function bt_confirm ( title, content, okCallback, okButton, cancelButton, options ) {
		
		var $dlg = _modal_markup();
		
		options = options || {};
		
		$dlg.addClass( "modal-confirm" );
		
		if( title ) {
			$dlg.find( ".modal-header h3" ).text( title );
		}	
		if( content ) {
			$dlg.find( ".modal-body p" ).html( content );
		}
		
		$dlg.find( "a.modal-ok" ).click( function( e ) {
			e.preventDefault();
			if( okCallback ) {
				okCallback( $dlg );
				if( !options.keepOpen ) {
					$dlg.hide();
				}
			}
		});
		
		if( okButton ) {
			$dlg.find( "a.modal-ok" ).text( okButton );
		}
	
		if( cancelButton ) {
			$dlg.find( "a.modal-cancel" ).text( cancelButton );
		}
		
		if( options && options.beforeShowCallback ) {
			$dlg.on( "show", function() {
				options.beforeShowCallback( $dlg );
			});
		}
		
		if( options && options.afterShowCallback ) {
			$dlg.on( "shown", function() {
				options.afterShowCallback( $dlg );
			});
		}
			
		if( options && options.beforeHideCallback ) {
			$dlg.on( "hide", function() {
				options.beforeHideCallback( $dlg );
			});
		}
		
		if( options && options.afterHideCallback ) {
			$dlg.on( "hidden", function() {
				options.afterHideCallback( $dlg );
			});
		}
		
		
		$dlg.on( "hidden", function() {
			setTimeout( function() {
				$dlg.remove();
			}, 100 );
		});
		
		$dlg.appendTo( jQuery( document.body ) );
		
		setTimeout( function() {
			$dlg.modal( "show" );
		}, 100 );
		
		
		$dlg.hide = function() {
			$dlg.modal( "hide" );	
		};
		
		return $dlg;
	}
	
	function image_dimensions( img, callback ) {
		
		var $ = jQuery;
			
		if( navigator.userAgent.match(/firefox/i) ) {
			var clone = $("img")[0]; // Get my img elem
			var pic_real_width, pic_real_height;
			$("<img>") // Make in memory copy of image to avoid css issues
				.attr("src", $(img).attr("src"))
				.load(function() {
					pic_real_width = this.width;   // Note: $(this).width() will not
					pic_real_height = this.height; // work for in memory images.
					callback( pic_real_width, pic_real_height );
			});
		}
		else {
			callback( img.width, img.height );
		}
	}
	
	function auto_image_position( img, containerWidth, containerHeight, fill, autoHeight )
	{
		var $ = jQuery;
		
		
		image_dimensions( img, function( w, h ) {
		
			var ratio = w/h;
		
	/* debugging
			if( $(img).closest( ".image-detail" ).length != 0 ) {
				alert( w + "," + h );
			}
	*/
			
			autoHeight = autoHeight || false;
			
			var imgW = 0;
			var imgH = 0;
			
			if( autoHeight || ratio >= 1 ) {
				// horizontal
				imgW = fill? containerWidth : Math.min( containerWidth, w );
				
				//img.style.width = imgW + "px";
				//img.style.height = "auto";
				
				imgH = Math.floor( imgW / ratio );
			}
			else {
				// vertical
				imgH = fill? containerHeight : Math.min( containerHeight, h );
				//img.style.height = imgH + "px";
				//img.style.width = "auto";
				
				imgW = Math.floor( imgH * ratio );
			}
			
			if( fill ) {
				if( autoHeight || imgW < containerWidth ) {
					imgW = containerWidth;
					imgH = Math.floor( imgW / ratio );
				}
				if( imgH < containerHeight ) {
					imgH = containerHeight;
					imgW = Math.floor( imgH * ratio );
				}
			}
			img.style.width = imgW + "px";
			img.style.height = imgH + "px";
			img.style.maxWidth = imgW + "px"; // counter-act some WP themes
			img.style.visibility = "visible";
			
			// center it
			img.style.left = Math.floor( ( containerWidth - imgW ) / 2	) + "px";
			if( autoHeight ) {
				img.style.top = "0";
			}
			else {
				img.style.top = Math.floor( ( containerHeight - imgH ) / 2	) + "px";
			}
			
		});
	}
	
	function auto_image( img, margin, borderWidth, fill, autoHeight, rerun )
	{
		var $ = jQuery;
		
		rerun = ( rerun || false );
		
		if( !rerun && ( !img.parentNode || img.parentNode.offsetWidth == 0 ) ) {
			setTimeout( function() {
				// f** IE
				auto_image( img, margin, borderWidth, fill, autoHeight, true );
			}, 100 );
			return;
		}
		
		if( $(img).data( "auto" ) ) {
			return;
		}
	
		if( img.src && img.src.indexOf( "/progress" ) != -1 ) {
			return;
		}
			
		img.style.margin = "0";
		img.style.position = "absolute";
		img.style.display = "block";
		img.style.visibility = "visible";
		
		var b = isNaN( borderWidth ) ? 1 : borderWidth;
		var inner = img.parentNode;
		var outer = img.parentNode.parentNode;
		
		var outerW = outer.offsetWidth;
		var outerH = outer.offsetHeight;
		
		var containerWidth = outerW - margin*2 - b*2;
		var containerHeight = outerH - margin*2 - b*2;
		
		inner.style.width = containerWidth + "px";
		if( !autoHeight ) {
			inner.style.height = containerHeight + "px";
		}
		inner.style.position = "absolute";
		inner.style.left = margin + "px";
		inner.style.top = margin + "px";
		inner.style.overflow = "hidden";
		//inner.style.backgroundColor = "yellow";
		
		outer.style.position = "relative";
		
		if( img.src && img.src.indexOf( "/nophoto" ) != -1 ) {
			//img.onclick = null;
			//img.style.cursor = "default";
			fill = false;
		}
		
		var progressElem = img.parentNode.querySelectorAll( ".progress" )[0];
		if( progressElem ) {
			progressElem.parentNode.removeChild( progressElem );
		}
		
		auto_image_position( img, containerWidth, containerHeight, fill, autoHeight );
		
		if( autoHeight ) {
			var imgH = img.height;
			inner.style.height = imgH + "px";
			outer.style.height = imgH + (margin*2) + "px";
		}
	}
	
	function addErrorToFormControl( control, error, options ) {
	
		var $ = jQuery;
		
		var additionalClass = "field-error-tooltip-right";
		
		if( options && options.placement === "top" ) {
			additionalClass = "field-error-tooltip-top";
		}
		else if( options && options.placement === "left" ) {
			additionalClass = "field-error-tooltip-left";
		}
		else if( options && options.placement === "bottom" ) {
			additionalClass = "field-error-tooltip-bottom";
		}
		
		if( $( control ).closest( ".modal" ).length > 0 ) {
			additionalClass += " field-error-tooltip-modal";
		}
		
		var opts = {
			placement : "right",
			trigger : "focus",
			title : error,
			template: "<div class='tooltip field-error-tooltip " + additionalClass + "'><div class='tooltip-arrow'></div><div class='tooltip-inner'></div></div>"
		};
		$.extend( opts, options || {} );
		
		$( control ).addClass( "error" );
		try {
			if( error ) {
				$( control ).removeData( 'tooltip' );
				$( control ).tooltip( opts );
				$( control ).tooltip( "enable" );
				$( control ).tooltip( "show" );
			}
		}
		catch( e ) {
			console.log( e );
		}
	}
	
	function clearErrorFromFormControl( control ) {
		
		var $ = jQuery;
		
		$( control ).removeClass( "error" );
		try
		{
			$( control ).tooltip( "disable" );
			$( control ).tooltip( "hide" );
			$( control ).removeData( 'tooltip' );
		}
		catch( e ) {	
			console.log( e );
		}
	}
	
	function addErrorsToFormControls( $form, errors, options ) {
		for( var i=0; i<errors.length; ++i ) {
			if( errors[i] && errors[i].field && errors[i].message ) {
				var $c = $form.find( ":input[name=" + errors[i].field + "]" );
				if( $c.length > 0 ) {
					clearErrorFromFormControl( $c );
					addErrorToFormControl( $c, errors[i].message, options );
				}
			}
		}
	}
	
	function clearErrorsFromFormControls( $form ) {
		$form.find( ":input" ).each( function() {
			clearErrorFromFormControl( this );
		})
	}
		
	function showProgressMessage( $container, message ) {
		if( message ) {
			$container.html( message );
		}
		try {
			$container.pulse( {
				opacity: 0
			}, {
				duration : 1000, 
				pulses: 10
			})
		}
		catch( e ) {
			console.log( e );
		}
		finally {
			$container.css( "visibility", "visible" );
		}
	}
	
	function hideProgressMessage( $container ) {
		$container.css( "visibility", "hidden" );
	}
	
	function addCacheBuster( url ) {
		var idx = url.indexOf( "?" );
		if( idx == -1 ) {
			url += "?__c=" + Math.random();
		}
		else {
			if( url.lastIndexOf( "&" ) === url.length - 1 ) {
				url += "__c=" + Math.random();
			}
			else {
				url += "&__c=" + Math.random();
			}
		}
		return url;
	}
	
	return {
		
		getInternetExplorerVersion	: getInternetExplorerVersion,
		getIEVersion				: getIEVersion,
		
		showElement					: showElement,
		hideElement					: hideElement,
		
		scrollToTop					: scrollToTop,
		new_window					: new_window,
		
		autosize_textarea           : autosize_textarea,
		
		isEmail						: isEmail,
		isURL						: isURL,
		
		bt_alert					: bt_alert,
		bt_confirm					: bt_confirm,
		auto_image					: auto_image,
		addErrorToFormControl		: addErrorToFormControl,
		clearErrorFromFormControl 	: clearErrorFromFormControl,
		clearErrorsFromFormControls : clearErrorsFromFormControls,
		addErrorsToFormControls 	: addErrorsToFormControls,
		showProgressMessage 		: showProgressMessage,
		hideProgressMessage 		: hideProgressMessage,
		addCacheBuster 				: addCacheBuster
	
	};	
});

