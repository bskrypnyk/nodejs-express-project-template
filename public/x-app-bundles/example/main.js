require( [
	"js/app",
	"x-app-bundles/example/app-bundle",
	"backbone"
	], 
	function(
		App,
		Bundle,
		_backbone
	) {
	
	
		
	function showNoteList() {
		var notes = new App.models.NoteListModel();
		var notesView = new App.views.NotesView( {
			collection: notes
		});
		notesView.on( "rendered", function( view ) {
			console.log( "list rendered: " + view.cid );
		});
		App.layout.addView( notesView );
		notesView.fetch();
		//notes.fetch();	
	}
	
	function showNote( id ) {
		var model = new App.models.NoteModel();
		model.id = parseInt(id);
		var noteView = new App.views.NoteView( {
			model : model
		});
		App.layout.addView( noteView );
		noteView.fetch();
		//model.fetch();
	}
	
	function newNote() {
		
		var model = new App.models.NoteModel();
		var noteView = new App.views.NoteView( {
			model : model
		});
		App.layout.addView( noteView );
		noteView.render();
	}

	function feedback( routeName ) {
		console.log( "Routing '" + routeName + "'" );
	}
	
	var router = Backbone.Router.extend( {
		
		routes: {
			""             : "noteList",
			"notes"        : "noteList",
			"notes/new"    : "newNote",
			"notes/:id"    : "noteItem",
			"*other"       : "defaultRoute"
		},
		
		initialize: function( options ) {
			var _this = this;
			App.on( "item-details", function( id ) { _this.navigate( 'notes/' + id, true ); });
		},
		
		noteList : function() {
			showNoteList();
		}
		,
		noteItem : function( id ) {
			showNote( id );
		}
		,
		newNote : function() {
			newNote();
		}
		,
		defaultRoute : function() {
			// if we are here, let's get to a "sane" position
			this.navigate( "", true );
		}
	});
	
	App.router = router;
		
							
	jQuery( function($) {
				
		// router should be initialized after DOM load to account for IE hash reliance on iframe
		// see router documentation on backbone
		var r = new router();
		
		App.layout = new App.HistoryLayout( { el : "#main-content" });
		App.layout.render();
		
		Backbone.history.start( { pushState: true, root : "/example/" });
		
		$( "[data-route]").on( "click", function( e ) {
			e.preventDefault();
			Backbone.history.navigateURL( $(this).attr( "data-route" ) );
			return false;
		});

	});	
	
	return router;
});