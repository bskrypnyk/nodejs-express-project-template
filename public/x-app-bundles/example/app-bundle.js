define( [	
	"js/app", 
	"tmpl!x-app-bundles/example/note-list.html", 
	"tmpl!x-app-bundles/example/note-details.html", 
	"tmpl!x-app-bundles/example/edit-note-modal.html" ],
	
	function(
	
		App,
		listTmpl,
		noteDetailsTmpl,
		editNoteTmpl

	) {


	var jsonRoot = "/example/json/notes";
			
	App.models.NoteModel = App.BaseModel.extend( {
		urlRoot: jsonRoot,
		
		parse : function( data ) {

			if( data && data.note ) {
				return data.note;
			}
			else {
				return data;
			}
		}
	});
	
	App.models.NoteListModel = App.BaseCollection.extend( {
		model : App.models.NoteModel,
		url   : jsonRoot,
		
		parse : function( data ) {
			return data.notes;
		}
	});
	
	App.views.NoteView = App.BaseFormView.extend( {
	
		options : {
			fieldErrorPlacement : "top"
		},
		
		template : noteDetailsTmpl,	
		
		initialize: function() {
			var _this = this;
			this.model.on( "sync", function() {
				_this.render();
			});
		},
		title : function() {
			if( this.model.isNew() ) {
				return "New note";
			}
			else {
				return "Note " + this.model.id;	
			}
		},
		
		showForm : function() {
		
			var _this = this;
			
			var txt = this.$el.find( "h3" ).text();
			var input = $( "<input name='title' type='text'>" );
			input.val( txt );
			this.$el.find( "h3" ).empty().append( input );
			
						
			this.$el.find( ".content" ).wrapInner( "<textarea name='text'></textarea>" );
			common.autosize_textarea( this.$el.find( ".content textarea" )[0] );
			
			this.$el.find( ".edit-toolbar" ).addClass( "hide" );
			this.$el.find( ".save-toolbar" ).removeClass( "hide" );

			this.$el.find( "h3 input" ).focus();
		},
		
		hideForm : function() {
		
			common.clearErrorsFromFormControls( this.$el );	
			this.render();
		},
		
		getFieldsToSubmit : function() {
			
			var title = this.$el.find( "h3 input" ).val();
			var content = this.$el.find( ".content textarea" ).val();
			
			return {
				title: title,
				text : content
			};	
		},
		
		submitSuccess: function() {
			if( this.wasNew() ) {
				Backbone.history.replaceURL( "/notes/" + this.model.id );
			}
		},
		
		render : function() {
			
			var _this = this;
			
			this.$el.html( this.template( this.model.toJSON() ) );
			
			this.$el.find( "a" ).on( "click", function( e ) {
				e.preventDefault();
				Backbone.history.navigateURL( $(this).attr( "href" ) );
				return false;
			});
			
			this.$el.find( ".edit-note-button" ).on( "click", function(e) {
				_this.showForm();
				return false;
			});
			
			this.$el.find( ".cancel-note-button" ).on( "click", function(e) {
				_this.hideForm();
				return false;
			});
			
			this.$el.find( ".save-note-button" ).on( "click", function(e) {
				_this.submit();
				return false;
			});
			
			if( this.model.isNew() ) {
				this.showForm();
			}
		}
	});
	
	App.views.NotesView = App.BaseView.extend( {
		tagName : "ul",
		className : "note-list",
		template : listTmpl,
		
		title : function() {
			return "Notes";
		},
		
		loadingMessage : function() {
			return "Loading notes...";
		},
		
		events : {
			"mouseenter li.note-item"  : "hoverover",
			"mouseleave li.note-item"  : "hoverout",
			"click li.note-item"       : "itemClicked"
		},
		
		initialize : function( options ) {
			//this.collection.on( "reset", function() {
			//	this.render();
			//}, this );
			
			var _this = this;
			
			this.collection.on( "change", function( model, changes ) {
				var id = model.get("id");
				var found = _this.$el.find( "li.note-item" ).filter( function() {
					return ( $(this).attr( "data-id" ) == id );
				});
				if( found.length > 0 ) {
					var updated = _this.template( [ model.toJSON() ] );	
					found.replaceWith( updated );	
				}
			});
		},
		
		title : function() {
			return "Notes";
		},
		
		hoverover : function( e ) {
			var $t = $(e.target);
			$t = $t.hasClass( "note-item") ? $t : $t.closest( "li.note-item" );
			$t.addClass( "hover" );
		},
		
		hoverout : function( e, param ) {
			var $t = $(e.target);
			$t = $t.hasClass( "note-item") ? $t : $t.closest( "li.note-item" );
			$t.removeClass( "hover" );
		},
		
		itemClicked : function( e ) {			
		
			var $t = $(e.target);
			$t = $t.hasClass( "note-item") ? $t : $t.closest( "li.note-item" );
			
			var id = $t.attr( "data-id" );
			
			if( true ) {
				App.trigger( "item-details", id );
				return;
			}
			
			var note = this.collection.find( function(note) { return note.id == id; } );
			
			var editor = new App.views.EditNoteView( { model : note } );
			editor.render();
		},
		
		render : function() {
			this.$el.html( this.template( this.collection.toJSON() ) );
		}
		
	});
	
	
	App.views.EditNoteView = App.BaseFormView.extend( {
		
		tagName : "div",
		className : "modal fade",
		role : "dialog",
		template : editNoteTmpl,

		initialize : function( options ) {
		
			_.extend( this.events, {
				"click .submit"   : "submit"
			})
		},		
		
		title : function() {
			if( model.get( "title" ) ) {
				return model.get( "title" );
			}
			else {
				return "Note";
			}
		},
		
		getFieldsToSubmit : function() {
			return {
				title : this.$el.find( ":input[name=title]" ).val(),
				text : this.$el.find( ":input[name=text]" ).val()
			}	
		},
		
		submitSuccess : function( model ) {
			this.$el.modal( "hide" );
		},
				
		render : function() {
		
			this.$el.append( $(this.template( this.model.toJSON())) );
			
			var _this = this;
						
			this.$el.modal({				
			})
			.on( "shown", function() {
				_this.$el.find( "input").eq(0).focus();
			})
			.on("hidden", function() {
				_this.remove();
			});
		}

	});
});