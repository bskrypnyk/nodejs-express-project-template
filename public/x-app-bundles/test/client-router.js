define( ["backbone"], function() {
	
	function feedback( routeName ) {
		console.log( "Routing '" + routeName + "'" );
		jQuery( "#route-feedback" ).html( "... Routing " + routeName + " ..." );
	}
	
	var router = Backbone.Router.extend( {
		
		routes: {
			"one"       : "routeOne",
			"two"       : "routeTwo",
			"home"      : "home",
			"about"     : "about",
			"contact"   : "contact",
			"*other"    : "defaultRoute"
		},
		
		routeOne : function() {
			feedback( "one" );
		}
		,
		routeTwo : function() {
			feedback( "two" );
		}
		,
		home : function() {
			feedback( "home" );
		}
		,
		about : function() {
			feedback( "about" );
		}
		,
		contact : function() {
			feedback( "contact" );
		}
		,
		defaultRoute : function() {
			feedback( "default" );
		}
	});
	
	return router;
});