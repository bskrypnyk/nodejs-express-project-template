require( [ 
	"js/app", 
	"x-app-bundles/test/client-router" ], 
	
	function( 
		app,
		Router
		) {
							
	jQuery( function($) {
	
		// router should be initialized after DOM load to account for IE hash reliance on iframe
		// see router documentation on backbone
		var r = new Router();
		Backbone.history.start( { pushState: true, root : "/test/" });
		
		$( "[data-route]").click( function() {
			r.navigate( $(this).attr( "data-route" ), true );
		});
		
		$( "[data-alert]" ).click( function() {
			var text = $(this).attr( "data-alert" );
			var title = $(this).attr( "data-alert-title" );
			common.bt_alert( title, text );
		});
		
		setTimeout( function() {
			require( [ "tmpl!/x-app-bundles/test/test-tmpl.html" ], function( tmpl ) {
				$( "#main" ).append( $( tmpl( { greeting: 'hello' } ) ) );
			});
		 }, 50 );
	});	
});
