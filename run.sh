#!/bin/bash

DIR="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd $DIR

echo "Running app..."
echo "=============================="
hash node 2>/dev/null || { echo >&2 "Please install node.  Aborting."; exit 1; }
node ./app.js