function nocache( req, res, next ) {

	res.header( 'Cache-Control', 'no-store, no-cache, must-revalidate' );
	res.setHeader('Pragma', 'no-cache');
	next();
}

exports.init = function( app ) {
	app.all( [ '/test', '/test/*' ], nocache, require( app.dir + '/routes/test/test.js' )(app) );
	
	// example bundle
	app.all( [ '/example/json/notes', '/example/json/notes/' ], nocache, require( app.dir + '/routes/example/example-api.js' )(app) );
	app.all( '/example/json/notes/:id', nocache, require(  app.dir + '/routes/example/example-api.js' )(app) );
	app.all( [ '/example', '/example/*' ], nocache, require(  app.dir + '/routes/example/example.js' )(app) );
	
};