var mock = require( 'mock-json' );
var form_support = require( _globals.basedir + '/lib/form-support.js' );

var item_tmpl = {
	'id|+1'           : 0,
	'title|3-10'      : '@LOREM ',
	'text|5-10'       : '@LOREM_IPSUM',
	'created'         : '@DATE_TIME',
	'modified'        : '@DATE_TIME',
	'related|1-5'     : [
		'/notes/@NUMBER@NUMBER@NUMBER'	
	]
};

var list_tmpl = {
	'notes|1-10'      : [
		
		item_tmpl
	
	]
};

var app = null;

var saved_notes = {
	
};

var simulateTimeout = 1500;

function handleList( req, res ) {
	
	setTimeout( function() {
		if( false && ( ( Math.floor( Math.random() * 10 ) ) % 9 ) == 0 ) {
			// random error
			res.send( 403, { errorMessage : "Random error: No notes" } );
		}
		else {
			res.send( 200, mock.generate( list_tmpl ));
		}
		
	}, 2000 );
	
}

function handleItem( req, res ) {

	var id = req.params.id;
	
	app.cache.get( 'notes.' + id, function( key, val ) {
		
		if( val ) {
			return res.send( 200, val );
		}
		else {
			r = mock.generate( item_tmpl );
			r.id = parseInt(id);
			return res.send( 200, r );
		}
		
	});
}

function handle( req, res, next ) {

	console.log( 'id: ' + req.param( 'id' ) );
	if( req.method === 'GET' ) {
		if( req.path.match( /\/notes[/]{0,1}$/i ) ) {
			return handleList( req, res );
		}
		else if( req.param( 'id' ) ) {
			return handleItem( req, res );
		}
	}
	else if( req.method === 'POST' ) {
		var json = req.body;
		
		setTimeout( function() {
			var out = { success: true, message: 'example api POST called' };
			out.note = json;
			out.note.id = Math.floor( Math.random() * 100 );
									
			if( !out.note.title || !out.note.text ) {
				var errors = form_support.createErrorResponse();
				errors.setErrorMessage( 'Gotta have something to say!' );
				if( !out.note.title ) {
					errors.addError( 'title', 'Say something, yo!' );
				}
				if( !out.note.text ) {
					errors.addError( 'text', 'Any notes, yo?' );
				}
				return res.send( 403, errors.getErrors() );
			}
			
			app.cache.put( 'notes.' + out.note.id, out.note );
			
			res.send( out );
		}, simulateTimeout );
	}
	else if( req.method === 'PUT' ) {
	
		// body is already an object converted from json		
		var json = req.body;
		
		setTimeout( function() {
		
			if( json && json.title === 'boo' ) {
				var errors = form_support.createErrorResponse();
				errors.setErrorMessage( 'don\'t boo me!' );
				errors.addError( 'title', 'Bad manners, yo!' );
				res.send( 403, errors.getErrors() );
			}
			else {
				if( !json.title || !json.text ) {
					var errors = form_support.createErrorResponse();
					errors.setErrorMessage( 'Gotta have something to say!' );
					if( !json.title ) {
						errors.addError( 'title', 'Say something, yo!' );
					}
					if( !json.text ) {
						errors.addError( 'text', 'Any notes, yo?' );
					}
					return res.send( 403, errors.getErrors() );
				}
	

				var out = { success: true, message: 'example api PUT called' };
				out.note = json;
				app.cache.put( 'notes.' + out.note.id, out.note );
				res.send( out );
			}
			
		}, simulateTimeout );
	}
	else if( req.method === 'DELETE' ) {
		console.log( 'example api DELETE called' );
		setTimeout( function() {
			res.send( { success: true, messag: 'example api DELETE called' } );
		}, simulateTimeout );
	}
}

module.exports = function( _app ) {
	app = _app;
	return handle;
};