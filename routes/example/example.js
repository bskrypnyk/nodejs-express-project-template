function handle( req, res, next ) {
	res.render( 'example/example.html', { req : req } );
}

module.exports = function( app ) {
	return handle;
};