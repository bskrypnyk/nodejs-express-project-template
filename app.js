/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes/routes.js')
  , fs = require('fs')
  , path = require( 'path' )
  , swig = require('swig')
  , cons = require( 'consolidate' )
  , http = require('http')
  , https = require('https')
  , dust = require( 'dustjs-linkedin' ) // optional
  , colocated_resources = require( './lib/static-colocated.js' )
  , less_middleware = require('less-middleware')
  , utils = require( './lib/utils.js' );
  
var app_defaults = require( 'config' ).app;
var server_defaults = require( 'config' ).server;

var app = express();  // GLOBAL: instantiate the express app

app.dir = __dirname; // anchor base directory to the "app"

initUploadDir();

app.engine( 'html', cons.swig );
//app.engine( 'html', cons.dust );

var swig_options = {
	root: __dirname + '/routes',
    allowErrors: true,
    autoescape: true,
    cache: false,
    encoding: 'utf-8',
    filters: {},
    tags: {},
    extensions: {},
    tzOffset: 0,
    filters: require( './lib/swig-filters.js' )
};

var less_options = {
	src: __dirname + '/public',
	debug: false,
	compress: true
};

app.configure('development', function(){
	swig_options['cache'] = false;
	less_options['compress'] = false;
	swig.init( swig_options );
});

app.configure('production', function(){
	less_options['once'] = true;
	swig_options.cache = true; // enable cache in production
	swig.init( swig_options );
});

// create and freez globals
_globals = {
	basedir : __dirname
};

// freezing globals
Object.freeze( _globals );



app.configure(function(){

	app.set('port', process.env.PORT || server_defaults.port);
	app.set('views', __dirname + '/routes');
	app.set('view engine', 'html');

	// below is used to capture the beginning of the request to measure start/end times
	app.use(function( req, res, next ) {
		req._startime = new Date().getTime();
		next();
	});
	
	app.use(express.favicon());
	app.use(express.logger('dev'));
	
	//app.use(express.json());
	//app.use(express.urlencoded());
	// bodyParser is equivalent to: .json() .urlencoded() .multipart()
	// leave out .multipart() as we handle uploads via formidable directly
	app.use(express.bodyParser());
	
	
	app.use(express.cookieParser());
	app.use(express.methodOverride());	
	app.use(express.compress( {
		filter : function(req, res){			
			var len = parseInt( res.getHeader( 'Content-Length' ) );
			if( !isNaN( len ) && len < 1024 * 4 ) {
				return false; // do not compress less than 4K
			}
			return /json|text|javascript/.test(res.getHeader('Content-Type'));
		}
	}));
	
	app.use(function (req, res, next) {
	    res.removeHeader("X-Powered-By"); // get rid of express header
	    next();
	});
	
	app.use( require( './lib/normalize-url' ) );
	
	// put it before middleware as we have a cheap testing for pre conditions, thus avoiding double hit 
	// on verifying if less needs to compile
	/* 
		DO NOT USE co-located resources for js files managed via require.js: 
		These resources will only work when they are non optimized. Optimizer will break
		on attempting to pull these resources from co-located directories
	*/
	colocated_resources.init( app, {
		client_side_url_prefix : '/res',
		server_side_route_subdir : '/res'
	} );

	app.use(less_middleware( less_options ));
	
	app.use(express.static(__dirname + '/public', { maxAge: app_defaults.static_resource_max_age } ));	
		
	app.use(app.router);
});

app.configure('development', function(){
	app.use(express.errorHandler());
});

app.locals( { 
	app_defaults: app_defaults,
	view_root : app.get('views')
} );

app.configure('production', function(){
	app.locals( { '_dist': '/dist' } );
});

/*
app.locals.use( function( req, res, done ) {	
	res.locals.somethingLocal = function() {
		return "dynamic";
	};
	done();
});
*/

routes.init( app );

// to launch in debug mode, use "node --debug ./app.js --debug"
// you need to repeat the --debug param for app.js to see it
var inDebug = false;
for( var i=0; i<process.argv.length; ++i ) {
	if( process.argv[i] === "--debug" ) {
		inDebug = true;
		break;
	}
}

function ensureCache( cache, type ) {
	if( !cache[type] ) {
		cache[type] = {};
	}
	return;
}

if( !inDebug ) {
	
	var cluster = require('./simplecluster').start();
	if (cluster.cluster.isMaster) {
	//  //do master stuff if anything at all
	
		// master side of the cache
		utils.initMasterCache( cluster );
		return;
	}
	
	// worker part of the cache
	app.cache = utils.initWorkerCache();
}
else {
	app.cache = utils.initSingleInstanceCache();
}


http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});

if( server_defaults.ssl && !inDebug ) {
	
	https.createServer({
		
		ca : fs.readFileSync( server_defaults.ssl_pem ),
		key: fs.readFileSync( server_defaults.ssl_key ),
		cert : fs.readFileSync( server_defaults.ssl_crt  )
		
	}, app ).listen(server_defaults.ssl_port, function() {
	  
		console.log("Express SSL server listening on port " + server_defaults.ssl_port);
	});		
}

function initUploadDir()
{
	// make sure temp dir for uploads exists
	if( !fs.existsSync( app_defaults.upload_dir ) ) {
		try {
			fs.mkdirSync( app_defaults.upload_dir );
		}
		catch( e ) {
			throw new Error( "ERROR: Failed to initialize upload dir: " + app_defaults.upload_dir + ": " + e.message );
		}
	}
	// make sure we can write to it
	try {
		fs.openSync( app_defaults.upload_dir + path.sep + "tmp", "w" );
	}
	catch( e ) {
		throw new Error( "ERROR: Upload dir: " + app_defaults.upload_dir + " is not writable: " + e.message );
	}
	
	try {
		fs.unlinkSync( app_defaults.upload_dir + path.sep + "tmp" );
	}
	catch( e ) {
		// there could be a race condition with all the worker instances, ignore
	}
}