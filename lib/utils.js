var appDefaults = require( 'config' ).app;

exports.niceErrorText = function( errorText ) {

	if( !errorText.match( /\.$/ ) ) {
		errorText += '.';
	}
	return errorText + ' If this error persists, please contact <a href="mailto:"' +
		appDefaults.support_email + '">' + appDeafaults.support_email + '</a>';
};

exports.urlToLog = function( req ) {
	var ip_address = '';
	if(req.headers['x-forwarded-for']){
	    ip_address = req.headers['x-forwarded-for'];
	}
	else {
	    ip_address = req.connection.remoteAddress;
	}
	return req.url + ' from: ' + ip_address + ' method: ' + req.method;
};

exports.timelog = function( req, label ) {
	console.log( label + ': ' + ( new Date().getTime() - req._startime ) + ' ms' );
};


exports.initMasterCache = function( cluster ) {

	var storage = {};
		
	Object.keys(cluster.workers).forEach(function(id) {
		cluster.workers[id].on('message', function(msg) {
			if( !msg || !msg.type || !msg.id ) {
				return;
			}
			if( msg.type === '_cache_get' ) {
				var key = msg.key;
				
				key = key || '';
				var val = storage[key];
				
				this.send( {
					id : msg.id,
					type : '_cache_get',
					key : key,
					val : val
				});
			}
			else if( msg.type === '_cache_put' ) {
				var key = msg.key;
				var val = msg.val;
				if( key ) {
					if( !val ) {
						delete storage[key];
					}
					else {
						storage[key] = val;
					}
				}
				this.send( {
					id : msg.id,
					type : '_cache_put',
					key : key,
					val : val
				});
			}
			else if( msg.type == '_cache_clear' ) {
				var key = msg.key;
				if( key ) {
					delete storage[key];
				}
				this.send( {
					id : msg.id,
					type : '_cache_clear',
					key: key
				});
			}
		});
	});	
};

exports.initWorkerCache = function() {

	var sources = {};
	var uniqueId = 1;
	
	function createMessage( type, key, val ) {
		
		return {
			type : type,
			id : ++uniqueId,
			key : key ? key : null,
			val : val ? val : null
		};
	}
	
	process.on( 'message', function( msg ) {
		
		if( !msg || !msg.id ) {
			return;
		}
		
		var callback = sources[''+msg.id];
		if( callback ) {
			callback( msg );
			delete sources[''+msg.id];
		}
		
		/*
		console.log( '+++++++++++++++++++++++++' );
		Object.keys(sources).forEach( function( key ) {
			console.log( key, sources[key] );
		});
		console.log( '+++++++++++++++++++++++++' );
		*/
	});
	
	return {
	
		put : function( key, val, callback ) {
			var msg = createMessage( '_cache_put', key, val );
			sources[''+msg.id] = function(msg) {
				if( callback ) {
					callback( msg.key, msg.val );
				}
			};
			process.send( msg );
		},
		
		get : function( key, callback ) {
			var msg = createMessage( '_cache_get', key );
			sources[''+msg.id] = function( msg ) {
				if( callback  ) {
					callback( msg.key, msg.val );
				}	
			};
			process.send( msg );
		},
		
		clear : function( key, callback ) {
			var msg = createMessage( '_cache_clear', key );
			sources[''+msg.id] = function( msg ) {
				if( callback ) {
					callback( msg.key );
				}	
			};
			process.send( msg );
		}
	};	
};

exports.initSingleInstanceCache = function() {

	var storage = {};
	
	return {
	
		put : function( key, val, callback ) {
		
			if( !val ) {
				delete storage[key];
			}			
			else {
				storage[key] = val;
			}
			
			if( callback ) {
				callback( key, val );
			}
		},
		
		get : function( key, callback ) {
			
			if( !key ) {
				if( callback ) {
					callback( key, null );
				}
				return;
			}
			
			var val = storage[key];
			if( callback ) {
				callback( key, val );
			}
		},
		
		clear : function( key, callback ) {
			if( key ) {
				delete storage[key];
			}
			if( callback ) {
				callback( key );
			}
		}
	};	
};