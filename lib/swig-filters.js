exports.split = function( input, delim ) {
	if (typeof input === 'string') {
		return input.toString().split( delim );
	}
	else {
		return [];
	}
};

exports.splitOnSpace = function( input ) {
	if (typeof input === 'string') {
		return input.toString().split( ' ' );
	}
	else {
		return [];
	}
};

exports.index_of = function( input, what ) {

	return input.indexOf( what );
};

exports.if_in_array = function( input, what, content ) {
	
	if( input.indexOf ) {
		if( input.indexOf( what ) != -1 ) {
			return content;
		}
	}
	
	return '';
};

exports.modulo = function( input, mod ) {
	
	if( typeof input === 'number' ) {
		return input % mod;
	}
	else {
		return 0;
	}
};

exports.is_date_blank = function( input ) {
	
	if( typeof input === 'date' ) {
		return ( input.getTime() === 0 );
	}
	return true;
}