var _ = require( 'underscore' );

exports.createErrorResponse = function() {
		
	var out = {
		errors : []
	};
	
	return {
		
		setErrorMessage : function( msg ) {
			out.errorMessage = msg;
		},
		getErrorMessage : function() {
			return out.errorMessage;
		},
		
		addError : function( field, message ) {
			if( !field || !message ) {
				return;
			}
			out.errors.push( {
				field: field,
				message: message
			});
		},
		
		getFieldErrors : function() {
			return out.errors;
		},
		
		removeError : function( field ) {
			_.each( out.errors, function( item, index ) {
				if( item && item.field === field ) {
					out.errors[index] = null;
					return false;	
				}
			});
			out.errors = _.compact( out.errors );
		},
		
		getErrors : function() {
			return out;
		},
		
		toJSON : function() {
			return JSON.stringify( out );
		}
	};
};