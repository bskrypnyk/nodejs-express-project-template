var URL = require( 'url' );

// collapse the '..' and '.' as well as double '//'
module.exports = function( req, res, next ) {
	
	var url = URL.parse(req.url);
	req.parsed_url = url;
	
	var path = unescape(req.path);
	var segments = path.split( /\// );
	var sanitized = [];
	var redirRequired = false;
	for( var i=0; i<segments.length; ++i ) {
		if( segments[i] == '.' ) {
			redirRequired = true;
			continue;
		}
		if( segments[i] == '..' ) {
			sanitized.pop();
			redirRequired = true;
		}
		else {
			sanitized.push( segments[i] );
		}
	}
	
	if( redirRequired ) {
		var location = sanitized.join( '/' );
		if( url.query ) {
			location += '?' + url.query;
		}
		res.redirect( 302, location );
		return;
	}
	next();
};