/**
	the purpose of this is to have a middleware that can serve
	static files from the '/routes' directory. It is nice to be able to
	consolidate all the static resources relevant to a route controller in
	the same folder, so everything that is relevant is co-located: css/less and js included
	
	on the client the URL should be formatted thus:
	
	/res/[controller]/[file.ext]
	
	and for less files:
	
	/res/[controller]/[file.ext]
*/

var _ = require( 'underscore' );
var events = require( 'events' );
var fs = require( 'fs' );
var send = require( 'send' );
var app_defaults = require( 'config' ).app;
var utils = require( './utils.js' );

var less = require('less-middleware');
var less_middleware = null;

var options = {
	
	colocated_root : '/routes', // for less_middleware
	debug: false, // for less_middleware
	compress: true, // for less_middleware
	client_side_url_prefix : '/res',
	server_side_route_subdir : '/res'
};

function pre_conditions( req ) {
	
	if( !req.path ) {
		return false;
	}
	var p = req.path.toLowerCase();

	if( !p.indexOf( options.client_side_url_prefix + '/' ) == 0 ) {
		return false;
	}
	
	return true;
}

function less_colocated( req, res, next ) {

	// BILL: keep in mind that the less_middleware WILL COMPILE the less
	// files at server startup EVEN IF THE DEST CSS FILE EXISTS AND IS NEWER THAN LESS
	// this is important to remember, as EACH child of the cluster will need to do same
	// until each is "hydrated". This may show through as a "slow" loading for the css
	// resource: first request - always; subsequent will depend if a "hydrated" instance
	// has been hit or not; eventuall all should be "hydrated"

	if( !pre_conditions( req ) ) {
		return next();
	}
	
	if( !req.path.match( /\.css$/i ) ) {
		return next();
	}
	
	var less_req = _.clone( req );
	
	var idx = req.url.indexOf( options.client_side_url_prefix + '/' );
	if( idx == -1 ) {
		return next();
	}
	
	var before = req.url.substring( 0, idx );
	var after = req.url.substring( idx + (options.client_side_url_prefix + '/' ).length );
	
	idx = after.indexOf( '/' );
	if( idx == -1 ) {
		return next();
	}
	
	// splice the server_side_route_subdir into path
	after = after.substring( 0, idx ) + options.server_side_route_subdir + after.substring( idx ); // [controller] + '/' + server-side-subdir + [remainder]
	
	less_req.url = before + '/' + after;
			
	less_middleware( less_req, res, next );
}

function static_colocated( req, res, next ) {

	if( !pre_conditions( req ) ) {
		return next();
	}
	
	var p = req.path.toLowerCase();
	
	var path = p.substring( ( options.client_side_url_prefix + '/' ).length );
	var idx = path.indexOf( '/' );
	if( idx ==-1 ) {
		return next(); // not found
	}
	var controller = path.substring( 0, idx );
	path = path.substring( idx );
	
	if( !path ) {
		return next(); // not found
	}

	path = '/' + controller + options.server_side_route_subdir + path;	
	
	send( req, path )
		.root( __dirname + '/..' + options.colocated_root )
		.maxage( app_defaults.static_resource_max_age || 0 )
		.hidden( function() {
			next(); // bail
		})
		.on( 'error', function() {
			next(); // bail
		})
		.on( 'directory', function() {
			next(); // bail
		})
		.pipe( res );
};

exports.init = function( app, opts ) {
	
	if( opts ) {
		_.extend( options, opts );
	}
	
	options.src = __dirname + '/..' + options.colocated_root;

	less_middleware = less( options );
	
	app.use( less_colocated );
	app.use( static_colocated );
};