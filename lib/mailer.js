var smtpConfig = require( 'config' ).smtp;
var nodemailer = require("nodemailer");

exports.sendEmail = function( to, from, subject, text, html, callback ) {

	var transport = nodemailer.createTransport("SMTP", smtpConfig );
	
	var email = {};
	if( to ) email.to = to;
	if( from ) email.from = from;
	if( subject ) email.subject = subject;
	if( text ) email.text = text;
	if( html ) email.html = html;
	
	transport.sendMail( email, function( err, result ) {
		if( callback ) {
			callback( err, result );
		}
	});	
};