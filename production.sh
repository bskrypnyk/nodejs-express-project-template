#!/bin/bash

DIR="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd $DIR

echo "Optimizing javascript files..."
echo "=============================="
/bin/bash ./optimize-js.sh || exit 1;
hash node 2>/dev/null || { echo >&2 "Please install node.  Aborting."; exit 1; }

echo ""
echo ""
echo "Running app..."
echo "=============================="
NODE_ENV=production node ./app.js