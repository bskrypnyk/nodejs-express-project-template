({
    //appDir: "./public",
    baseUrl: "public/",
	dir : "./public/dist",
	paths 		: {
		// note: no .js extensions here
		"text"				: 	"js/_requirejs/text",
		"tmpl"              :   "js/_common/tmpl-require-plugin",
		"i18n"              :   "js/_requirejs/i18n",
		"underscore"		:	"js/_external/underscore-min",
		"jquery"			:	"js/_external/jquery-1.7.1.min",
		"jquery_form"		: 	"js/_external/jquery.form",
		"jquery_transit"	:	"js/_external/jquery.transit.min",
		"jquery_pulse"		: 	"js/_external/jquery.pulse.min",
		"jquery_cookies"	: 	"js/_external/jquery.cookies.2.2.0.min",
		"json2"				:	"js/_external/json2",
		"bootstrap"			:	"bootstrap/js/bootstrap",
		"backbone"			:	"js/_external/backbone",
		"locale"			:	"js/_common/locale",
		"common"			:	"js/_common/common",
		"fileupload-custom"	:	"js/_common/fileupload-custom",
		"swig"              :	"js/_external/swig"
	},
	mainConfigFile : "./public/js/require-config.js",
	optimizeCss : "none",
	//optimize: "none",
    modules: [
        {
            name: "x-app-bundles/test/main"
        }
        ,
        {
            name: "x-app-bundles/example/main"
        }
    ]
})
