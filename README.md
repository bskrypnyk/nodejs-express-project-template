Node.js web application template
============================

Based on "express". Work in progress.

Includes built-in support for:

* cluster
* ssl + regular server
* debug fallback to single instance
* cofiguration support via "config" package
* LESS support
* compression support
* server-side templating via SWIG (and dustjs, linkedin variant)
* includes dependency on mysql db driver and pool library for connection pooling
* master process based cache
* require.js support
* backone.js support, base classes, utils, etc.


### SWIG : less known things
*   use "_context" if you need to refer to the top level context as a variable, e.g. if you pass array into template, you can use:  

    ``{% for item in _context %}...{% endfor %}``

*   next item....